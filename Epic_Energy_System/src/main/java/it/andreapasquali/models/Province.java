package it.andreapasquali.models;

import javax.persistence.Column;
import javax.persistence.Entity;

import it.andreapasquali.models.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//@author Andrea

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Province extends BaseEntity {

	@Column(nullable = false, length = 80)
	private String name;
	//Acronimo della fattura
	@Column(nullable = false, length = 2)
	private String acronym;
	private Province province;

}
