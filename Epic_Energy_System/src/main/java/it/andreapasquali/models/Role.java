package it.andreapasquali.models;

import javax.persistence.Entity;
import javax.persistence.Table;

import it.andreapasquali.models.entities.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//@author Andrea

@Entity
@Table(name = "role")
@Getter
@Setter
@NoArgsConstructor
public class Role extends BaseEntity {

	//Ruolo che lo User può assumere, dato che può essere aggiornato è una stringa
	private String roleType;

}