package it.andreapasquali.models;

import javax.persistence.Entity;

import it.andreapasquali.models.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//@author Andrea, Stato della fattura

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class InvoicesStatus extends BaseEntity {

	// Gli stati fattura ( informazione, fattura pagata, fattura emessa, fattura non
	// pagata) possono essere dinamici, in quanto in base all'evoluzione del
	// business possono essere inseriti nel sistema nuovi stati.
	private String status;

}
