package it.andreapasquali.models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import it.andreapasquali.models.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//@author Andrea, Indirizzo

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Address extends BaseEntity {

	//Via
	private String street;
	//Numero civico
	private String streetNumber;
	//Località/posto
	private String locality;
	//Codice postale
	private int cap;
	//Istanza della città
	@ManyToOne
	private City city;
	//Istanza del client
	@ManyToOne
	private Client client;

}
