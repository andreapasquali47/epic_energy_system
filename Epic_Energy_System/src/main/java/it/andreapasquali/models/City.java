package it.andreapasquali.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import it.andreapasquali.models.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//@author Andrea, città

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class City extends BaseEntity {

	//Nome 
	@Column(nullable = false, length = 125)
	private String name;
	//Capitale, valore booleano per determinare la presenza o l'assenza (T or F)
	private boolean capital;
	//Provincia della città
	@ManyToOne(cascade = CascadeType.ALL)
	private Province province;

}
