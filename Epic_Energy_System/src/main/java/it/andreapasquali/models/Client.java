package it.andreapasquali.models;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import it.andreapasquali.models.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//@author Andrea

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Client extends BaseEntity {

	//Nome
	private String name;
	//Nome dell'azienda (ragione sociale)
	private String companyName;
	//Numero IVA
	private int IVAnumber;
	//Email azienda
	private String email;
	//Data di inserimento dei dati
	private int insertDate;
	//Data di ultimo contatto
	private Date lastContactDate;
	//Reddito annuale
	private BigDecimal annualRevenue;
	//Pec
	private String pec;
	//Numero di telefono
	private long phoneNumber;
	//Istanza della classe contatto che contiene le informazioni
	@ManyToOne(cascade = CascadeType.ALL)
	private Contact contact;
	//Indirizzo della sede legale
	@OneToOne(cascade = CascadeType.ALL)
	private Address legalAddress;
	//Indirizzo della sede operativa
	@OneToOne(cascade = CascadeType.ALL)
	private Address operativeAddress;
	//Tipo di azienda/cliente
	@ManyToOne(cascade = CascadeType.ALL)
	private ClientTypes clientTypes;

}
