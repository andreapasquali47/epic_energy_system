package it.andreapasquali.models.entities;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//Superclasse, la sua funzione è di poter definire deglia attributi che verranno poi implementati in altre classi.

@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public class BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column( //
			columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", //
			insertable = false, //
			updatable = false)
	private Date createdAt;

}
