package it.andreapasquali.models.pojo;

import java.math.BigDecimal;

import it.andreapasquali.models.InvoicesStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//POJO (Plain Old Java Object), questo oggetto è un ordinario Java, non un oggetto speciale

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoicePojo {

	private int id;
	private Integer year;
	private String date;
	private Integer number;
	private BigDecimal importo;
	private InvoicesStatus statusName;
	private int idClient;
}
