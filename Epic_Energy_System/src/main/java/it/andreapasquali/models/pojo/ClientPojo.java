package it.andreapasquali.models.pojo;

import java.sql.Date;

import it.andreapasquali.models.Address;
import it.andreapasquali.models.ClientTypes;
import it.andreapasquali.models.Contact;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//POJO (Plain Old Java Object), questo oggetto è un ordinario Java, non un oggetto speciale

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientPojo {

	private String name;
	private String companyName;
	private int IVAnumber;
	private String email;
	private Date lastContactDate;
	private String pec;
	private long phoneNumber;
	private Address legalAddress;
	private Address operativeAddress;
	private Contact contact;
	private ClientTypes clientTypes;
}
