package it.andreapasquali.models.pojo;

import it.andreapasquali.models.City;
import it.andreapasquali.models.Client;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//POJO (Plain Old Java Object), questo oggetto è un ordinario Java, non un oggetto speciale

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressPojo {
	private int id;
	private String street;
	private String streetNumber;
	private String locality;
	private int cap;
	private City city;
	private Client client;
}
