package it.andreapasquali.models.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//POJO (Plain Old Java Object), questo oggetto è un ordinario Java, non un oggetto speciale

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPojo {

	private int id;
	private String username;
	private String email;
	private String password;
    private String name;
    private String surname;
    private String role;
}
