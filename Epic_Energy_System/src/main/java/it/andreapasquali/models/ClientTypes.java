package it.andreapasquali.models;

import javax.persistence.Entity;

import it.andreapasquali.models.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//@author Andrea

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ClientTypes extends BaseEntity {

	// i client possono essere di diverso tipo: 
	// -PA(pubblica amministrazione) -SAS -SPA -SRL
	private String clientTypes;

}
