package it.andreapasquali.models;

import javax.persistence.Entity;

import it.andreapasquali.models.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//@author Andrea

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Contact extends BaseEntity {

	//Dati del contatto
	private String email;
	//Nome
	private String name;
	//Cognome
	private String surname;
	//Numero di telefono
	private Long phoneNumber;
}
