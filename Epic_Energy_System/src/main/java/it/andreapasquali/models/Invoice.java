package it.andreapasquali.models;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

import it.andreapasquali.models.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//@author Andrea, Fattura

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Invoice extends BaseEntity {

	//Anno della fattura
	private Integer year;
	//Data della fattura
	private LocalDate date;
	//Importo della fattura
	private BigDecimal importo;
	//Numero della fattura
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer number;
	//A quale Client la fattura è legata
	@ManyToOne(cascade = CascadeType.ALL)
	private Client client;
	//Indica lo stato della fattura
	@ManyToOne(cascade = CascadeType.ALL)
	private InvoicesStatus invoicesStatus;

}
