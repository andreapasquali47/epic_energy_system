package it.andreapasquali.services;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.andreapasquali.models.Client;
import it.andreapasquali.models.Invoice;
import it.andreapasquali.models.InvoicesStatus;

public interface InvoiceService {

	public Invoice add(Invoice invoice);

	public Invoice update(Invoice invoice);

	public Invoice delete(Invoice invoice);

	public Page<Invoice> getByYear(Integer year, Pageable pageable);

	public Page<Invoice> getByDate(LocalDate date, Pageable pageable);

	public Page<Invoice> getByImport(BigDecimal importo, Pageable pageable);

	public List<Invoice> getByClient(Client client);

	public Page<Invoice> getByInvoicesStatus(InvoicesStatus invoicesStatus, Pageable pageable);

	public Page<Invoice> getByImportoBetween(BigDecimal firstImport, BigDecimal secondImport, Pageable pageable);

}
