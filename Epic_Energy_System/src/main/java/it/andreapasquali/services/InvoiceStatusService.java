package it.andreapasquali.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.andreapasquali.models.InvoicesStatus;

public interface InvoiceStatusService {

	public InvoicesStatus add(InvoicesStatus invoicesStatus);

	public InvoicesStatus update(InvoicesStatus invoicesStatus);

	public InvoicesStatus delete(InvoicesStatus invoicesStatus);

	public Page<InvoicesStatus> getByStatus(InvoicesStatus status, Pageable pageable);
}
