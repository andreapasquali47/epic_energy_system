package it.andreapasquali.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.Province;
import it.andreapasquali.repository.ProvinceRepository;
import it.andreapasquali.services.ProvinceService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class ProvinceServiceImpl implements ProvinceService {

	@Autowired
	ProvinceRepository provinceR;

	@Override
	public Province add(Province province) {
		try {
			return provinceR.save(province);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Province update(Province province) {
		try {
			Province p = provinceR.findById(province.getId()).get();
			if (province.getName() != null)
				if (!province.getName().isBlank())
					p.setName(province.getName());
			if (province.getAcronym() != null)
				if (!province.getAcronym().isBlank())
					p.setAcronym(province.getAcronym());
			return provinceR.save(province);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Province delete(Province province) {
		try {
			Province p = provinceR.findById(province.getId()).get();
			return provinceR.save(p);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Province> getByName(String name) {
		try {
			return provinceR.findByName(name);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Province> getByAcronym(String acronym) {
		try {
			return provinceR.findByAcronym(acronym);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

}
