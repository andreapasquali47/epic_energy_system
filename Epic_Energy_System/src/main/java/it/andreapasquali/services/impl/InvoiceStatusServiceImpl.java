package it.andreapasquali.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.InvoicesStatus;
import it.andreapasquali.repository.InvoicesStatusRepository;
import it.andreapasquali.services.InvoiceStatusService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class InvoiceStatusServiceImpl implements InvoiceStatusService {

	@Autowired
	InvoicesStatusRepository invoiceStatusR;

	@Override
	public InvoicesStatus add(InvoicesStatus invoicesStatus) {
		try {
			return invoiceStatusR.save(invoicesStatus);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public InvoicesStatus update(InvoicesStatus invoicesStatus) {
		try {
			InvoicesStatus is = invoiceStatusR.findById(invoicesStatus.getId()).get();
			if (invoicesStatus.getStatus() != null)
				if (!invoicesStatus.getStatus().isBlank())
					is.setStatus(invoicesStatus.getStatus());
			return invoiceStatusR.save(is);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public InvoicesStatus delete(InvoicesStatus invoicesStatus) {
		try {
			InvoicesStatus i = invoiceStatusR.getById(invoicesStatus.getId());
			invoiceStatusR.delete(i);
			return i;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<InvoicesStatus> getByStatus(InvoicesStatus status, Pageable pageable) {
		try {
			return invoiceStatusR.findByStatus(status, pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

}
