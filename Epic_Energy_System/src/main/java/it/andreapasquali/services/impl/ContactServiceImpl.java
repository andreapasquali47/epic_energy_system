package it.andreapasquali.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.Contact;
import it.andreapasquali.repository.ContactRepository;
import it.andreapasquali.services.ContactService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class ContactServiceImpl implements ContactService {

	@Autowired
	ContactRepository contactR;

	@Override
	public Contact add(Contact contact) {
		try {
			return contactR.save(contact);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Contact update(Contact contact) {
		try {
			var opt = contactR.findById((long) contact.getId());
			if (opt.isPresent()) {
				var updatable = opt.get();
				if (contact.getName() != null)
					updatable.setName(contact.getName());
				if (contact.getSurname() != null)
					updatable.setSurname(contact.getSurname());
				if (contact.getEmail() != null)
					updatable.setEmail(contact.getEmail());
				if (contact.getPhoneNumber() != null)
					updatable.setPhoneNumber(contact.getPhoneNumber());
				return contactR.save(updatable);
			}
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
		return contact;
	}

	@Override
	public Contact delete(Contact contact) {
		try {
			var c = contactR.getById((long) contact.getId());
			contactR.delete(c);
			return c;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Contact> getByEmail(String email) {
		try {
			return contactR.findByEmail(email);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Contact> getByName(String name) {
		try {
			return contactR.findByName(name);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Contact> getBySurname(String surname) {
		try {
			return contactR.findBySurname(surname);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Contact> getByPhoneNumber(Long phoneNumber) {
		try {
			return contactR.findByPhoneNumber(phoneNumber);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

}
