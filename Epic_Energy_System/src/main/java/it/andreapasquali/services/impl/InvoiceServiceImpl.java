package it.andreapasquali.services.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.Client;
import it.andreapasquali.models.Invoice;
import it.andreapasquali.models.InvoicesStatus;
import it.andreapasquali.repository.InvoiceRepository;
import it.andreapasquali.services.InvoiceService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	InvoiceRepository invoiceR;

	@Override
	public Invoice add(Invoice invoice) {
		try {
			return invoiceR.save(invoice);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Invoice update(Invoice invoice) {
		try {
			var opt = invoiceR.findById(invoice.getId());
			if (opt.isPresent()) {
				var updatable = opt.get();
				if (invoice.getImporto() != null)
					updatable.setImporto(invoice.getImporto());
				if (invoice.getInvoicesStatus() != null)
					updatable.setInvoicesStatus(invoice.getInvoicesStatus());
				if (invoice.getNumber() != null)
					updatable.setNumber(invoice.getNumber());
				return invoiceR.save(updatable);
			}
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
		return invoice;
	}

	@Override
	public Invoice delete(Invoice invoice) {
		try {
			var i = invoiceR.getById(invoice.getId());
			invoiceR.delete(i);
			return i;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Invoice> getByDate(LocalDate date, Pageable pageable) {
		try {
			return invoiceR.findByDate(date, pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Invoice> getByImport(BigDecimal importo, Pageable pageable) {
		try {
			return invoiceR.findByImporto(importo, pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	public List<Invoice> getByClient(Client client) {
		try {
			return invoiceR.findByClient(client);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	public Page<Invoice> getByInvoicesStatus(InvoicesStatus invoicesStatus, Pageable pageable) {
		try {
			return invoiceR.findByInvoicesStatus(invoicesStatus, pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	public Page<Invoice> getByImportoBetween(BigDecimal firstImport, BigDecimal secondImport, Pageable pageable) {
		try {
			return invoiceR.findByImportoBetween(firstImport, secondImport, pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Invoice> getByYear(Integer year, Pageable pageable) {
		try {
			return invoiceR.findByYear(year, pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

}
