package it.andreapasquali.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.User;
import it.andreapasquali.repository.RoleRepository;
import it.andreapasquali.repository.UserRepository;
import it.andreapasquali.services.UserService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userR;
	@Autowired
	RoleRepository roleR;
	@Autowired
	PasswordEncoder encoder;

	@Override
	public User add(User user) {
		try {
			User u = new User();
			u.setName(user.getName());
			u.setSurname(user.getSurname());
			u.setEmail(user.getEmail());
			u.setUsername(user.getUsername());
			u.setPassword(user.getPassword());
			return userR.save(user);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public User delete(User user) {
		try {
			User u = userR.getById(user.getId());
			if (!u.getRole().equals(roleR)) {
				userR.delete(u);
			}
			return u;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public User update(User user) {
		try {
			User u = userR.findById(user.getId()).get();
			if (user.getName() != null)
				if (!user.getName().isBlank())
					u.setName(user.getName());
			if (user.getSurname() != null)
				if (!user.getSurname().isBlank())
					u.setSurname(user.getSurname());
			if (user.getUsername() != null)
				if (!user.getUsername().isBlank())
					u.setUsername(user.getUsername());
			if (user.getPassword() != null)
				if (!user.getPassword().isBlank())
					u.setPassword(user.getPassword());
			if (user.getEmail() != null)
				if (!user.getEmail().isBlank())
					u.setEmail(user.getEmail());
			return userR.save(u);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<User> getByName(String name) {
		try {
			return userR.findByName(name);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	public List<User> getBySurname(String surname) {
		try {
			return userR.findBySurname(surname);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<User> getByUsername(String username) {
		try {
			return userR.findByUsername(username);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<User> getByPassword(String password) {
		try {
			return userR.findByPassword(password);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Optional<User> getByEmail(String email) {
		try {
			return userR.findByEmail(email);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	public Page<User> getAllCityPageAndSort(Integer page, Integer size, String dir, String sort) {
		try {
			return userR.findAllCityPageAndSort(page, size, dir, sort);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public void criptAllPasswords() {

		List<User> list = userR.findAll();

		for (User u : list) {
			String hashedPassword = encoder.encode(u.getPassword());
			u.setPassword(hashedPassword);
			userR.save(u);
		}

	}

}
