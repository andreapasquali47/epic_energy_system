package it.andreapasquali.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.Address;
import it.andreapasquali.models.Client;
import it.andreapasquali.repository.AddressRepository;
import it.andreapasquali.repository.ClientRepostory;
import it.andreapasquali.services.AddressService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	AddressRepository addressR;
	@Autowired
	ClientRepostory clientR;

	@Override
	public Address add(Address address) {
		try {
			return addressR.save(address);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Address update(Address address) {
		try {
			Address u = addressR.findById(address.getId()).get();
			if (address.getCap() != 0)
				u.setCap(address.getCap());
			if (address.getStreet() != null)
				u.setStreet(address.getStreet());
			if (address.getStreetNumber() != null)
				u.setStreetNumber(address.getStreetNumber());
			if (address.getLocality() != null)
				u.setLocality(address.getLocality());
			return addressR.save(u);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Address delete(Address address) {
		try {
			Address u = addressR.getById(address.getId());
			addressR.delete(u);
			return u;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Address> getByStreet(String street) {
		try {
			return addressR.findByStreet(street);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Address> getByStreetNumber(String streetNumber) {
		try {
			return addressR.findByStreetNumber(streetNumber);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Address> getByLocality(String locality) {
		try {
			return addressR.findByLocality(locality);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Address> getByCap(int cap) {
		try {
			return addressR.findByCap(cap);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Address> getByClient(Client client) {
		try {
			var c = clientR.findById(client.getId());
			return addressR.findByClient(c);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Address> getAll(Integer size, Integer page, String dir, String sort) {
		try {
			return addressR.findAll(size, page, dir, sort);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

}
