package it.andreapasquali.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.City;
import it.andreapasquali.models.Province;
import it.andreapasquali.repository.CityRepository;
import it.andreapasquali.repository.ProvinceRepository;
import it.andreapasquali.services.CityService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class CityServiceImpl implements CityService {

	@Autowired
	CityRepository cityR;
	@Autowired
	ProvinceRepository provinceR;

	@Override
	public City add(City city) {
		try {
			Province province = null;
			@SuppressWarnings("null")
			var p = provinceR.findByAcronym(province.getProvince().getAcronym());
			if (p.isEmpty())
				province = provinceR.save(province.getProvince());
			else
				province = p.get(0);
			province.setProvince(province);
			return cityR.save(city);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public City update(City city) {
		try {
			var opt = cityR.findById(city.getId());
			if (opt.isPresent()) {
				var updatable = opt.get();
				if (city.getName() != null)
					updatable.setName(city.getName());
				if (city.isCapital())
					updatable.setCapital(city.isCapital());
				if (city.getProvince() != null)
					updatable.setProvince(city.getProvince());
				return cityR.save(updatable);
			}
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
		return city;
	}

	@Override
	public City delete(City city) {
		try {
			var c = cityR.getById(city.getId());
			cityR.delete(c);
			return c;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<City> getByName(String name) {
		try {
			return cityR.findByName(name);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<City> getByCapital(boolean capital) {
		try {
			return cityR.findByCapital(capital);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<City> getByProvince(Province province) {
		try {
			return cityR.findByProvince(province);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Province> getProvinces(Pageable pageable) {
		try {
			return provinceR.findAll(pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<City> getAllCityPageAndSort(Pageable pageable) {
		try {
			return cityR.findAll(pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	public Page<City> getAllCityPageAndSort(Integer page, Integer size, String dir, String sort) {
		try {
			return cityR.findAllCityPageAndSort(page, size, dir, sort);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

}
