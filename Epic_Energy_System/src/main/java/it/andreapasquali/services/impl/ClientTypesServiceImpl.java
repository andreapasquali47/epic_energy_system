package it.andreapasquali.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.ClientTypes;
import it.andreapasquali.repository.ClientTypesRepository;
import it.andreapasquali.services.ClientTypesService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class ClientTypesServiceImpl implements ClientTypesService {

	@Autowired
	ClientTypesRepository clientTypesR;

	@Override
	public ClientTypes add(ClientTypes clientTypes) {
		try {
			return clientTypesR.save(clientTypes);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public ClientTypes update(ClientTypes clientTypes) {
		try {
			var opt = clientTypesR.findById(clientTypes.getId());
			if (opt.isPresent()) {
				var updatable = opt.get();
				if (clientTypes.getClientTypes() != null)
					updatable.setClientTypes(clientTypes.getClientTypes());
				return clientTypesR.save(updatable);
			}
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
		return clientTypes;
	}

	@Override
	public ClientTypes delete(ClientTypes clientTypes) {
		try {
			var c = clientTypesR.getById(clientTypes.getId());
			clientTypesR.delete(c);
			return c;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<ClientTypes> getByClientTypes(String clientTypes) {
		try {
			return clientTypesR.findByClientTypes(clientTypes);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

}
