package it.andreapasquali.services.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.City;
import it.andreapasquali.models.Client;
import it.andreapasquali.repository.ClientRepostory;
import it.andreapasquali.services.ClientService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	ClientRepostory clientR;

	@Override
	public Client add(Client client) {
		try {
			return clientR.save(client);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Client update(Client client) {
		try {
			var opt = clientR.findById(client.getId());
			if (opt.isPresent()) {
				var updatable = opt.get();
				if (client.getEmail() != null)
					updatable.setEmail(client.getEmail());
				if (client.getCompanyName() != null)
					updatable.setCompanyName(client.getCompanyName());
				if (client.getName() != null)
					updatable.setName(client.getName());
				if (client.getIVAnumber() != 0)
					updatable.setIVAnumber(client.getIVAnumber());
				if (client.getInsertDate() != 0)
					updatable.setInsertDate(client.getInsertDate());
				if (client.getLastContactDate() != null)
					updatable.setLastContactDate(client.getLastContactDate());
				if (client.getAnnualRevenue() != null)
					updatable.setAnnualRevenue(client.getAnnualRevenue());
				if (client.getPec() != null)
					updatable.setPec(client.getPec());
				if (client.getPhoneNumber() != 0)
					updatable.setPhoneNumber(client.getPhoneNumber());
				return clientR.save(updatable);
			}
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
		return client;
	}

	@Override
	public Client delete(Client client) {
		try {
			var c = clientR.getById(client.getId());
			clientR.delete(c);
			return c;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Client> getAllByOrderByName(Integer page, Integer size, String dir, String sort) {
		try {
			return clientR.findAllByOrderByName(page, size, dir, sort);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Client> getByAnnualRevenue(BigDecimal annualRevenue, Pageable pageable) {
		try {
			return clientR.findByAnnualRevenue(annualRevenue, pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Client> getByInsertDate(Date insertDate, Pageable pageable) {
		try {
			return clientR.findByCreatedAt(insertDate, pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Client> getByLastContactDate(Date lastContactDate, Pageable pageable) {
		try {
			return clientR.findByLastContactDate(lastContactDate, pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	public Client getById(long idClient) {
		try {
			return clientR.findByIdClient(idClient);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	public ResponseEntity<?> update(long contactId, long clientTypeId) {
		try {
			return clientR.findByIdClientAndContact(contactId, clientTypeId);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Optional<Client> delete(long id) {
		try {
			return clientR.findById((int) id);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<City> getAllCityPageAndSort(Integer page, Integer size, String dir, String sort) {
		try {
			return clientR.findAllCityPageAndSort(page, size, dir, sort);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Client> getAllByOrderByAnnualRevenue(Integer page, Integer size, String dir, String sort) {
		try {
			return clientR.findAllByOrderByAnnualRevenue(page, size, dir, sort);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Client> getAllByOrderByCreatedAt(Integer page, Integer size, String dir, String sort) {
		try {
			return clientR.findAllByOrderByCreatedAt(page, size, dir, sort);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Client> getAllByOrderByLastContactDate(Integer page, Integer size, String dir, String sort) {
		try {
			return clientR.findAllByOrderByLastContactDate(page, size, dir, sort);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

}
