package it.andreapasquali.services;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.andreapasquali.models.City;
import it.andreapasquali.models.Client;

public interface ClientService {

	public Client add(Client client);

	public Client update(Client client);

	public Client delete(Client client);

	public Page<Client> getAllByOrderByName(Integer page, Integer size, String dir, String sort);

	public Page<Client> getAllByOrderByAnnualRevenue(Integer page, Integer size, String dir, String sort);

	public Page<Client> getAllByOrderByCreatedAt(Integer page, Integer size, String dir, String sort);

	public Page<Client> getAllByOrderByLastContactDate(Integer page, Integer size, String dir, String sort);

	public Page<Client> getByAnnualRevenue(BigDecimal annualRevenue, Pageable pageable);

	public Page<Client> getByInsertDate(Date insertDate, Pageable pageable);

	public Page<Client> getByLastContactDate(Date lastContactDate, Pageable pageable);

	public Optional<Client> delete(long id);

	public Page<City> getAllCityPageAndSort(Integer page, Integer size, String dir, String sort);

}
