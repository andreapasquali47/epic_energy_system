package it.andreapasquali.services;

import java.util.List;

import it.andreapasquali.models.Contact;

public interface ContactService {

	public Contact add(Contact contact);

	public Contact update(Contact contact);

	public Contact delete(Contact contact);

	public List<Contact> getByEmail(String email);

	public List<Contact> getByName(String name);

	public List<Contact> getBySurname(String surname);

	public List<Contact> getByPhoneNumber(Long phoneNumber);
}
