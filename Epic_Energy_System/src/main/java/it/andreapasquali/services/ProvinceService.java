package it.andreapasquali.services;

import java.util.List;

import it.andreapasquali.models.Province;

public interface ProvinceService {

	public Province add(Province province);

	public Province update(Province province);

	public Province delete(Province province);

	public List<Province> getByName(String name);

	public List<Province> getByAcronym(String acronym);
}
