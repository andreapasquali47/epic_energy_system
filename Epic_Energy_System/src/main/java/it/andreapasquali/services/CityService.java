package it.andreapasquali.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.andreapasquali.models.City;
import it.andreapasquali.models.Province;

public interface CityService {

	public City add(City city);

	public City update(City city);

	public City delete(City city);

	public List<City> getByName(String name);

	public List<City> getByCapital(boolean capital);

	public List<City> getByProvince(Province province);

	public Page<Province> getProvinces(Pageable unpaged);

	public Page<City> getAllCityPageAndSort(Pageable pageable);
}
