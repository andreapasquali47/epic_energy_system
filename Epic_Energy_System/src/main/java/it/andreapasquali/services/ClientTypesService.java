package it.andreapasquali.services;

import java.util.List;

import it.andreapasquali.models.ClientTypes;

public interface ClientTypesService {

	public ClientTypes add(ClientTypes clientTypes);

	public ClientTypes update(ClientTypes clientTypes);

	public ClientTypes delete(ClientTypes clientTypes);

	public List<ClientTypes> getByClientTypes(String clientTypes);

}
