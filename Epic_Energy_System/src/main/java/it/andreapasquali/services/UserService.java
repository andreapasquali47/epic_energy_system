package it.andreapasquali.services;

import java.util.List;
import java.util.Optional;

import it.andreapasquali.models.User;

public interface UserService {

	public List<User> getByName(String name);

	public List<User> getBySurname(String surname);

	public List<User> getByUsername(String username);

	public List<User> getByPassword(String password);

	public Optional<User> getByEmail(String email);

	public User add(User user);

	public User delete(User user);

	public User update(User user);

	void criptAllPasswords();
}
