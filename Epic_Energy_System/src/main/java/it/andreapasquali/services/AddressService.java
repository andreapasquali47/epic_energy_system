package it.andreapasquali.services;

import java.util.List;

import org.springframework.data.domain.Page;

import it.andreapasquali.models.Address;
import it.andreapasquali.models.Client;

public interface AddressService {

	public Address add(Address address);

	public Address update(Address address);

	public Address delete(Address address);

	public List<Address> getByStreet(String street);

	public List<Address> getByStreetNumber(String streetNumber);

	public List<Address> getByLocality(String locality);

	public List<Address> getByCap(int cap);

	public List<Address> getByClient(Client client);

	public Page<Address> getAll(Integer size, Integer page, String dir, String sort);
}
