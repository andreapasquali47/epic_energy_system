package it.andreapasquali.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.andreapasquali.models.ClientTypes;

public interface ClientTypesRepository extends JpaRepository<ClientTypes, Integer> {

	public List<ClientTypes> findByClientTypes(String clientTypes);

}
