package it.andreapasquali.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.Address;
import it.andreapasquali.models.Client;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {

	public List<Address> findByStreet(String street);

	public List<Address> findByStreetNumber(String streetNumber);

	public List<Address> findByLocality(String locality);

	public List<Address> findByCap(int cap);

	public List<Address> findByClient(Optional<Client> c);

	public Page<Address> findAll(Integer size, Integer page, String dir, String sort);

}
