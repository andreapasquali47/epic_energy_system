package it.andreapasquali.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import it.andreapasquali.models.InvoicesStatus;

public interface InvoicesStatusRepository extends JpaRepository<InvoicesStatus, Integer> {

	public Page<InvoicesStatus> findByStatus(InvoicesStatus status, Pageable pageable);

}
