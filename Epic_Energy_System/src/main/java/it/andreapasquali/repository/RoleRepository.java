package it.andreapasquali.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

	public Page<Role> findByRoleType(String roleType);
}
