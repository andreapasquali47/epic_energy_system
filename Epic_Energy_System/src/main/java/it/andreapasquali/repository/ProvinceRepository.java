package it.andreapasquali.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.Province;

@Repository
public interface ProvinceRepository extends JpaRepository<Province, Integer> {

	// recupera una provincia tramite la sigla
	public List<Province> findByAcronym(String acronym);

	public List<Province> findByName(String name);
}
