package it.andreapasquali.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	public List<User> findByName(String name);

	public List<User> findBySurname(String surname);

	public List<User> findByUsername(String username);

	public List<User> findByPassword(String password);

	public Optional<User> findByEmail(String email);

	public Page<User> findAllCityPageAndSort(Integer page, Integer size, String dir, String sort);

}
