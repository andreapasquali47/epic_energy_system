package it.andreapasquali.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.City;
import it.andreapasquali.models.Province;

@Repository
public interface CityRepository extends JpaRepository<City, Integer> {

	public List<City> findAllByProvinceAcronym(String acronym);

	public List<City> findByName(String name);

	public List<City> findByCapital(boolean capital);

	public List<City> findByProvince(Province province);

	public Page<City> findAllCityPageAndSort(Integer page, Integer size, String dir, String sort);
}
