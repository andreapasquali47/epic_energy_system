package it.andreapasquali.repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.Client;
import it.andreapasquali.models.Invoice;
import it.andreapasquali.models.InvoicesStatus;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {

	public List<Invoice> findByClient(Client client);

	public Page<Invoice> findByInvoicesStatus(InvoicesStatus invoicesStatus, Pageable pageable);

	public Page<Invoice> findByDate(LocalDate date, Pageable pageable);

	public Page<Invoice> findByYear(int year, Pageable pageable);

	public Page<Invoice> findByImportoBetween(BigDecimal firstImport, BigDecimal secondImport, Pageable pageable);

	public Page<Invoice> findByImporto(BigDecimal importo, Pageable pageable);

}
