package it.andreapasquali.repository;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.City;
import it.andreapasquali.models.Client;

@Repository
public interface ClientRepostory extends JpaRepository<Client, Integer> {

	// get all
	public Page<Client> findAllByOrderByName(Integer page, Integer size, String dir, String sort);

	public Page<Client> findAllByOrderByAnnualRevenue(Integer page, Integer size, String dir, String sort);

	public Page<Client> findAllByOrderByCreatedAt(Integer page, Integer size, String dir, String sort);

	public Page<Client> findAllByOrderByLastContactDate(Integer page, Integer size, String dir, String sort);
	// public Page<Client> findAllByTotalSalesBetween(BigDecimal firstImport,
	// BigDecimal secondImport, Pageable pageable);

	// get by
	public Page<Client> findByAnnualRevenue(BigDecimal annualRevenue, Pageable pageable);

	public Page<Client> findByCreatedAt(Date createdAt, Pageable pageable);

	public Page<Client> findByLastContactDate(Date lastContactDate, Pageable pageable);

	public Page<Client> findByName(String name, Pageable pageable);

	public Client findByIdClient(long idClient);

	public ResponseEntity<?> findByIdClientAndContact(long contactId, long clientTypeId);

	public Page<City> findAllCityPageAndSort(Integer page, Integer size, String dir, String sort);

}
