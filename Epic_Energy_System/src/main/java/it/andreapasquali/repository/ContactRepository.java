package it.andreapasquali.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.andreapasquali.models.Contact;

public interface ContactRepository extends JpaRepository<Contact, Long> {

	public List<Contact> findByEmail(String email);

	public List<Contact> findByName(String name);

	public List<Contact> findBySurname(String surname);

	public List<Contact> findByPhoneNumber(Long phoneNumber);

}
