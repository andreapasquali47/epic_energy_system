package it.andreapasquali.runner;

import java.io.FileInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import be06.epicode.cities.CitiesLoader;
import it.andreapasquali.models.City;
import it.andreapasquali.models.Client;
import it.andreapasquali.models.Province;
import it.andreapasquali.services.impl.CityServiceImpl;

@Component
public class Runner implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(Runner.class);

	@Autowired
	CityServiceImpl citySI;

	@Override
	public void run(String... args) throws Exception {
		var s = Client.builder().name("").companyName("").IVAnumber(342242).email("").build();
		log.info("Client: {}", s);

		if (citySI.getProvinces(Pageable.unpaged()).isEmpty()) {
			var fileName = "C:\\Users\\Andrea\\git\\EpicEnergySystem\\Epic_Energy_System\\Elenco-comuni-italiani.csv";
			var cities = CitiesLoader.load(new FileInputStream(fileName));
			log.info("Loaded {} cities", cities.size());
			cities.stream()
					.map(c -> City.builder().name(c.getName())
							.province(Province.builder().name(c.getProvince().getName())
									.acronym(c.getProvince().getAcronym()).build())
							.build())
					.forEach(c -> citySI.add(c));
		}
	}
}
