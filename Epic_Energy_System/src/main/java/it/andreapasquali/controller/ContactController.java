package it.andreapasquali.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.andreapasquali.models.Contact;
import it.andreapasquali.services.impl.ContactServiceImpl;

@RestController
@RequestMapping("/contact")
public class ContactController {

	@Autowired
	ContactServiceImpl contactSI;

	@PostMapping(value = "/add")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Contact add(@RequestBody Contact contact) {
		Contact contacts = Contact.builder().email(contact.getEmail()).name(contact.getName())
				.surname(contact.getSurname()).phoneNumber(contact.getPhoneNumber()).build();
		return contactSI.add(contacts);
	}

	@PostMapping(value = "/update")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Contact update(@RequestBody Contact contact) {
		Contact contacts = Contact.builder().email(contact.getEmail()).name(contact.getName())
				.surname(contact.getSurname()).phoneNumber(contact.getPhoneNumber()).build();
		contacts.setId(contact.getId());
		return contactSI.update(contacts);
	}

	@PostMapping(value = "/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Contact delete(@RequestBody Contact contact) {
		return contactSI.delete(contact);
	}

}
