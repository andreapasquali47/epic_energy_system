package it.andreapasquali.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.andreapasquali.models.Address;
import it.andreapasquali.models.pojo.AddressPojo;
import it.andreapasquali.services.impl.AddressServiceImpl;
import it.andreapasquali.services.impl.ClientServiceImpl;

@RestController
@RequestMapping("/address")
public class AddressController {

	@Autowired
	AddressServiceImpl addressSI;
	@Autowired
	ClientServiceImpl clientSI;

	@PostMapping(value = "/add")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Address add(@RequestBody AddressPojo addressPojo) {
		Address address = Address.builder().street(addressPojo.getStreet()).streetNumber(addressPojo.getStreetNumber())
				.locality(addressPojo.getLocality()).cap(addressPojo.getCap()).client(addressPojo.getClient())
				.city(addressPojo.getCity()).build();
		return addressSI.add(address);
	}

	@PostMapping(value = "/update")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Address update(@RequestBody AddressPojo addressPojo) {
		Address address = Address.builder().street(addressPojo.getStreet()).streetNumber(addressPojo.getStreetNumber())
				.locality(addressPojo.getLocality()).cap(addressPojo.getCap()).client(addressPojo.getClient())
				.city(addressPojo.getCity()).build();
		address.setId(addressPojo.getId());
		return addressSI.update(address);
	}

}
