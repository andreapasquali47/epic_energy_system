package it.andreapasquali.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.andreapasquali.models.City;
import it.andreapasquali.services.impl.CityServiceImpl;

@RestController
@RequestMapping("/city")
public class CityController {

	@Autowired
	CityServiceImpl citySI;

	@PostMapping(value = "/addcity")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public City add(@RequestParam City city) {
		City cities = City.builder().name(city.getName()).build();
		return citySI.add(cities);
	}

	@PostMapping(value = "/getallcitypagesort")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hashRole('ROLE_USER')")
	public Page<City> getAllCityPageSort(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "2") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort) {
		Page<City> pag = citySI.getAllCityPageAndSort(page, size, dir, sort);
		return pag;
	}

	@PostMapping(value = "/updatecity")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public City update(@RequestBody City city) {
		City cities = City.builder().name(city.getName()).build();
		cities.setId(city.getId());
		return citySI.update(cities);
	}

	@PostMapping(value = "/deletecity")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public City delete(@RequestBody City city) {
		return citySI.delete(city);
	}

}
