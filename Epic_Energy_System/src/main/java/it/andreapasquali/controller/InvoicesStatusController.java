package it.andreapasquali.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.andreapasquali.models.InvoicesStatus;
import it.andreapasquali.services.impl.InvoiceStatusServiceImpl;

@RestController
@RequestMapping("/invoicestatus")
public class InvoicesStatusController {

	@Autowired
	InvoiceStatusServiceImpl invoiceStatusSI;

	@PostMapping(value = "/addstatus")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public InvoicesStatus add(@RequestBody InvoicesStatus invoicesStatus) {
		InvoicesStatus invoicesS = InvoicesStatus.builder().status(invoicesStatus.getStatus()).build();
		return invoiceStatusSI.add(invoicesS);
	}

	@PostMapping(value = "/updatestatus")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public InvoicesStatus update(@RequestBody InvoicesStatus invoicesStatus) {
		InvoicesStatus invoicesS = InvoicesStatus.builder().status(invoicesStatus.getStatus()).build();
		invoicesS.setId(invoicesStatus.getId());
		return invoiceStatusSI.update(invoicesS);
	}
}
