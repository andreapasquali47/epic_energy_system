package it.andreapasquali.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.andreapasquali.models.Client;
import it.andreapasquali.models.Invoice;
import it.andreapasquali.models.InvoicesStatus;
import it.andreapasquali.models.pojo.InvoicePojo;
import it.andreapasquali.services.impl.ClientServiceImpl;
import it.andreapasquali.services.impl.InvoiceServiceImpl;
import it.andreapasquali.services.impl.InvoiceStatusServiceImpl;

@RestController
@RequestMapping("/invoice")
public class InvoiceController {

	@Autowired
	InvoiceServiceImpl invoiceSI;
	@Autowired
	InvoiceStatusServiceImpl invoiceStatusSI;
	@Autowired
	ClientServiceImpl clientSI;

	@PostMapping(value = "/add")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Invoice add(@RequestBody InvoicePojo invoicePojo) throws ParseException {
		Invoice i = Invoice.builder().importo(invoicePojo.getImporto()).number(invoicePojo.getNumber())
				.year(invoicePojo.getYear()).invoicesStatus(invoicePojo.getStatusName())
				.client(clientSI.getById(invoicePojo.getId())).build();
		return invoiceSI.add(i);
	}

	@PostMapping(value = "/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Invoice update(@RequestBody InvoicePojo invoicePojo) {
		Invoice i = Invoice.builder().importo(invoicePojo.getImporto()).number(invoicePojo.getNumber())
				.year(invoicePojo.getYear()).invoicesStatus(invoicePojo.getStatusName())
				.client(clientSI.getById(invoicePojo.getId())).build();
		i.setId(invoicePojo.getId());
		return invoiceSI.update(i);
	}

	@PostMapping(value = "/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Invoice delete(@RequestBody Invoice invoice) {
		return invoiceSI.delete(invoice);
	}

	@PostMapping(value = "/getbydate")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hashRole('ROLE_USER')")
	public Page<Invoice> getByDate(@RequestParam LocalDate date, @RequestBody BigDecimal firstImport,
			@RequestBody BigDecimal secondImport, @RequestParam(defaultValue = "2") Integer page,
			@RequestParam(defaultValue = "2") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort) {
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		return invoiceSI.getByImportoBetween(firstImport, secondImport, paging);
	}

	@PostMapping(value = "/searchclient")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Invoice> getByClient(@RequestBody Client client) {
		if (client == null) {
			return null;
		}
		return invoiceSI.getByClient(client);
	}

	@PostMapping(value = "/getbyimport")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Invoice> getByImporto(@RequestParam BigDecimal importo, @RequestParam(defaultValue = "2") Integer page,
			@RequestParam(defaultValue = "2") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort) {
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		Page<Invoice> imports = invoiceSI.getByImport(importo, pageable);
		if (imports.isEmpty()) {
			return null;
		}
		return invoiceSI.getByImport(importo, pageable);
	}

	@PostMapping(value = "/getbydate")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Invoice> getByDate(@RequestParam LocalDate date, @RequestParam(defaultValue = "2") Integer page,
			@RequestParam(defaultValue = "2") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort) {
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		Page<Invoice> dates = invoiceSI.getByDate(date, pageable);
		if (dates.isEmpty()) {
			return null;
		}
		return invoiceSI.getByDate(date, pageable);
	}

	@PostMapping(value = "/getbyyear")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Invoice> getByYear(@RequestParam int year, @RequestParam(defaultValue = "2") Integer page,
			@RequestParam(defaultValue = "2") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort) {
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		Page<Invoice> years = invoiceSI.getByYear(year, pageable);
		if (years.isEmpty()) {
			return null;
		}
		return invoiceSI.getByYear(year, pageable);
	}

	@PostMapping(value = "/searchinvoicesstatus")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<InvoicesStatus> getByInvoicesStatus(@RequestBody InvoicesStatus invoicesStatus,
			@RequestParam(defaultValue = "2") Integer page, @RequestParam(defaultValue = "2") Integer size,
			@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort) {
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		Page<InvoicesStatus> status = invoiceStatusSI.getByStatus(invoicesStatus, pageable);
		if (status.isEmpty()) {
			return null;
		}
		List<InvoicesStatus> list = status.getContent();
		return invoiceStatusSI.getByStatus(list.get(0), pageable);
	}

}
