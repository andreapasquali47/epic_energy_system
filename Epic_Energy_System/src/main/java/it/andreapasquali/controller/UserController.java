package it.andreapasquali.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.andreapasquali.authorizations.JwtUtils;
import it.andreapasquali.models.User;
import it.andreapasquali.models.pojo.UserPojo;
import it.andreapasquali.repository.UserRepository;
import it.andreapasquali.services.impl.UserServiceImpl;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserServiceImpl userSI;
	@Autowired
	UserRepository userR;
	@Autowired
	JwtUtils jwtUtils;

	@PostMapping(value = "/getalluserpagesort")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hashRole('ROLE_USER')")
	public Page<User> getAllUserPageSort(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "10") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort) {
		Page<User> pag = userSI.getAllCityPageAndSort(page, size, dir, sort);
		return pag;
	}

	@PostMapping(value = "/add")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public User add(@RequestBody UserPojo userPojo) {
		User user = User.builder().username(userPojo.getUsername()).email(userPojo.getEmail())
				.password(userPojo.getPassword()).name(userPojo.getName()).surname(userPojo.getSurname()).build();
		return userSI.add(user);
	}

	@PostMapping(value = "/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public User update(@RequestBody UserPojo userPojo) {
		User user = User.builder().username(userPojo.getUsername()).email(userPojo.getEmail())
				.password(userPojo.getPassword()).name(userPojo.getName()).surname(userPojo.getSurname()).build();
		user.setId(userPojo.getId());
		return userSI.update(user);
	}

}
