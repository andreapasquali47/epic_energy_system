package it.andreapasquali.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.andreapasquali.models.Client;
import it.andreapasquali.models.pojo.ClientPojo;
import it.andreapasquali.services.impl.ClientServiceImpl;
import it.andreapasquali.services.impl.ContactServiceImpl;

@RestController
@RequestMapping("/client")
public class ClientController {

	@Autowired
	ClientServiceImpl clientSI;
	@Autowired
	ContactServiceImpl contactSI;

	@PostMapping(value = "/addclient")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Client addClient(@RequestBody ClientPojo clientPojo) {
		Client c = Client.builder().name(clientPojo.getName()).email(clientPojo.getEmail())
				.IVAnumber(clientPojo.getIVAnumber()).clientTypes(clientPojo.getClientTypes())
				.companyName(clientPojo.getCompanyName()).phoneNumber(clientPojo.getPhoneNumber())
				.lastContactDate(clientPojo.getLastContactDate()).legalAddress(clientPojo.getLegalAddress())
				.pec(clientPojo.getPec()).contact(clientPojo.getContact()).build();
		return clientSI.add(c);
	}

	@PostMapping(value = "/updateclient")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> updateClient(@PathVariable long contactId, @PathVariable long clientTypeId) {
		return ResponseEntity.ok(clientSI.update(contactId, clientTypeId));
	}

	@PostMapping(value = "/deleteclient")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Optional<Client> deleteClient(@RequestParam int id) {
		return clientSI.delete(id);
	}

	@GetMapping(value = "/getorderedbyname")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Client> getByContactName(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "10") Integer size, @RequestParam(defaultValue = "asc") String direction,
			@RequestParam(defaultValue = "name") String sort) {
		Page<Client> pag = clientSI.getAllByOrderByName(page, size, direction, sort);
		return pag;
	}

	@GetMapping(value = "/getorderedbyannualrevenue")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Client> getByAnnualRevenue(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "10") Integer size, @RequestParam(defaultValue = "asc") String direction,
			@RequestParam(defaultValue = "annual_revenue") String sort) {
		Page<Client> pag = clientSI.getAllByOrderByAnnualRevenue(page, size, direction, sort);
		return pag;
	}

	@GetMapping(value = "/getorderedbyinsertdate")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Client> getByInsertDate(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "10") Integer size, @RequestParam(defaultValue = "asc") String direction,
			@RequestParam(defaultValue = "insert_date") String sort) {
		Page<Client> pag = clientSI.getAllByOrderByCreatedAt(page, size, direction, sort);
		return pag;
	}

	@GetMapping(value = "/getorderedbylastcontactdate")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Client> getByLastContactDate(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "10") Integer size, @RequestParam(defaultValue = "asc") String direction,
			@RequestParam(defaultValue = "last_contact_date") String sort) {
		Page<Client> pag = clientSI.getAllByOrderByLastContactDate(page, size, direction, sort);
		return pag;
	}
}
