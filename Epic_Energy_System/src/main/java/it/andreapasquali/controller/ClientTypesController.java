package it.andreapasquali.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.andreapasquali.models.ClientTypes;
import it.andreapasquali.services.impl.ClientTypesServiceImpl;

@RestController
@RequestMapping("/clienttypes")
public class ClientTypesController {

	@Autowired
	ClientTypesServiceImpl clientTypesSI;

	@PostMapping(value = "addclienttypes")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ClientTypes add(@RequestBody ClientTypes clientTypes) {
		ClientTypes clientT = ClientTypes.builder().clientTypes(clientTypes.getClientTypes()).build();
		return clientTypesSI.add(clientT);
	}

	@PostMapping(value = "updateclienttypes")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ClientTypes update(@RequestBody ClientTypes clientTypes) {
		ClientTypes clientT = ClientTypes.builder().clientTypes(clientTypes.getClientTypes()).build();
		clientT.setId(clientTypes.getId());
		return clientTypesSI.update(clientT);
	}

	@PostMapping(value = "deleteclienttypes")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ClientTypes delete(@RequestBody ClientTypes clientTypes) {
		return clientTypesSI.delete(clientTypes);
	}
}
