package it.andreapasquali;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import it.andreapasquali.models.Address;
import it.andreapasquali.models.Client;
import it.andreapasquali.models.ClientTypes;
import it.andreapasquali.models.Contact;
import it.andreapasquali.repository.ClientRepostory;
import it.andreapasquali.services.impl.ClientServiceImpl;
import it.andreapasquali.services.impl.ClientTypesServiceImpl;
import it.andreapasquali.services.impl.UserServiceImpl;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EpicEnergySystemApplicationTests {

	@Autowired
	UserServiceImpl userSI;
	@Autowired
	ClientTypesServiceImpl clientTypesSI;
	@Autowired
	ClientServiceImpl clientSI;
	@Autowired
	ClientRepostory clientR;
	Client c;
	Contact co;
	Address ad;
	ClientTypes ct;

	@BeforeEach
	public void setup() {
		c = Client.builder().name("Wind").IVAnumber(23232).companyName("Wind Tre").email("wind@tre.it")
				.pec("wind@tre.it").phoneNumber(192193973L).build();
		co = Contact.builder().email("paolorossi@gmail.com").name("Paolo").surname("Rossi").build();
		ad = Address.builder().cap(45678).build();
		ct = ClientTypes.builder().clientTypes("Tipo").build();
	}

	@Test
	@DisplayName("Add client")
	void testAddClient() {
		clientSI.add(c);
		assertEquals(c, clientR.getById(1), "Client Added");
	}

}
